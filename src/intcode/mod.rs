//! This is the main module for the intcode computer and its related functionality.

use std::io::stdin;

/// A stack of ints.
type Stack = Vec<isize>;

/// An intcode computer.
/// # Examples
/// ```
/// use advent::intcode::Computer;
/// let cpu = Computer::new();
/// ```
pub struct Computer {
    /// Whether to print debug messages, useful for testing.
    debug: bool,

    /// The computer's stack.
    stack: Stack,

    /// Current instruction pointer.
    pointer: usize,
}

macro_rules! dprintln {
    ($self:expr, $fmt:expr) => (dprintln!($self, "{}", $fmt));
    ($self:expr, $fmt:expr, $($arg:tt)*) => {
        if $self.debug {
            eprintln!($fmt, $($arg)*);
        }
    }
}

impl Computer {
    /// Create a new, default computer.
    pub fn new() -> Computer {
        Computer::real_new(false)
    }

    /// Create a computer with debug turned on.
    pub fn debug() -> Computer {
        Computer::real_new(true)
    }

    /// Initialize a computer with the given debug value.
    fn real_new(debug: bool) -> Computer {
        Computer {
            debug: debug,
            stack: Vec::new(),
            pointer: 0,
        }
    }

    /// Load a program into memory.
    pub fn load_in(self: &mut Computer, input: Stack) {
        self.stack = input;
        dprintln!(self, "{} ITEMS LOADED", self.stack.len());
    }

    fn get_param(self: &Self, ptr_offset: usize) -> isize {
        let mode = self.get_mode(ptr_offset);
        match mode {
            0 => self.position_mode(ptr_offset),
            1 => self.immediate_mode(ptr_offset),
            x => panic!("INVALID MODE ({})", x),
        }
    }

    fn immediate_mode(self: &Self, ptr_offset: usize) -> isize {
        *self.stack.get(self.pointer + ptr_offset).unwrap()
    }

    fn position_mode(self: &Self, ptr_offset: usize) -> isize {
        let idx: isize = self.immediate_mode(ptr_offset) as isize;
        dprintln!(self, "IDX: {}", idx);
        *self.stack.get(idx as usize).unwrap()
    }

    fn write_out(self: &mut Self, out_param: usize, val: isize) {
        let out_i = self.immediate_mode(out_param);
        self.set(out_i as usize, val);
    }

    fn set(self: &mut Computer, idx: usize, val: isize) {
        self.stack[idx] = val;
    }

    // OP 1
    fn add(self: &mut Computer) {
        let inp1 = self.get_param(1);
        let inp2 = self.get_param(2);
        self.write_out(3, inp1 + inp2);
        self.pointer += 4;
    }

    // OP 2
    fn mult(self: &mut Computer) {
        let inp1 = self.get_param(1);
        let inp2 = self.get_param(2);
        self.write_out(3, inp1 * inp2);
        self.pointer += 4;
    }

    // OP 3
    fn read(self: &mut Self) {
        let mut inp = String::new();
        stdin().read_line(&mut inp).unwrap();
        self.write_out(1, inp.trim().parse().unwrap());
        self.pointer += 2;
    }

    // OP 4
    fn output(self: &mut Self) {
        let out = self.get_param(1);
        println!("{}", out);
        self.pointer += 2;
    }

    // OP 5
    fn jmp_true(self: &mut Self) {
        let inp1 = self.get_param(1);
        let inp2 = self.get_param(2);
        if inp1 != 0 {
            self.pointer = inp2 as usize;
        } else {
            self.pointer += 3;
        }
    }

    // OP 6
    fn jmp_false(self: &mut Self) {
        let inp1 = self.get_param(1);
        let inp2 = self.get_param(2);
        if inp1 == 0 {
            self.pointer = inp2 as usize;
        } else {
            self.pointer += 3;
        }
    }

    // OP 7
    fn less_than(self: &mut Self) {
        let inp1 = self.get_param(1);
        let inp2 = self.get_param(2);
        if inp1 < inp2 {
            self.write_out(3, 1);
        } else {
            self.write_out(3, 0);
        }
        self.pointer += 4;
    }

    // OP 8
    fn equals(self: &mut Self) {
        let inp1 = self.get_param(1);
        let inp2 = self.get_param(2);
        if inp1 == inp2 {
            self.write_out(3, 1);
        } else {
            self.write_out(3, 0);
        }
        self.pointer += 4;
    }

    fn instr(self: &Self) -> usize {
        self.immediate_mode(0) as usize
    }

    fn op_code(self: &Self) -> usize {
        self.instr() % 100
    }

    fn get_mode(self: &Self, param_num: usize) -> usize {
        let instr = self.instr();
        let modestr: usize = instr / 100;
        let divisor = (10 as usize).pow((param_num - 1) as u32);
        if divisor == 0 {
            0
        } else {
            (modestr / divisor) % 10
        }
    }

    fn cycle(self: &mut Computer) -> bool {
        let op_code = self.op_code();
        dprintln!(self, "OPCODE {}", op_code);
        match op_code {
            1 => {
                self.add();
            }

            2 => {
                self.mult();
            }

            3 => {
                self.read();
            }

            4 => {
                self.output();
            }

            5 => {
                self.jmp_true();
            }

            6 => {
                self.jmp_false();
            }

            7 => {
                self.less_than();
            }

            8 => {
                self.equals();
            }

            99 => {
                return false;
            }

            x => {
                panic!("UNSUPPORTED OPCODE ({})", x);
            }
        }
        true
    }

    /// Run the currently loaded program, and return all of the values in memory.
    pub fn run(self: &mut Computer) -> Stack {
        while self.cycle() {}
        self.stack.clone()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn assert_run(arr1: Vec<isize>, arr2: Vec<isize>) {
        let mut cpu = Computer::debug();
        cpu.load_in(arr1);
        let output = cpu.run();
        assert_eq!(output, arr2);
    }

    #[test]
    fn test_run() {
        assert_run(vec![1, 0, 0, 0, 99], vec![2, 0, 0, 0, 99]);
        assert_run(vec![2, 3, 0, 3, 99], vec![2, 3, 0, 6, 99]);
        assert_run(vec![2, 4, 4, 5, 99, 0], vec![2, 4, 4, 5, 99, 9801]);
        assert_run(
            vec![1, 1, 1, 4, 99, 5, 6, 0, 99],
            vec![30, 1, 1, 4, 2, 5, 6, 0, 99],
        );
    }

    #[test]
    fn test_neg() {
        assert_run(vec![1101, 100, -1, 4, 0], vec![1101, 100, -1, 4, 99]);
    }
}
