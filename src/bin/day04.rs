fn main() {
    println!("part1: {}", part1());
    println!("part2: {}", part2());
}

fn part1() -> u32 {
    let range = 231832..767346;
    let mut num_possible = 0;
    for n in range {
        if satisfies_part1(n.to_string()) {
            num_possible += 1;
        }
    }
    num_possible
}

fn part2() -> u32 {
    let range = 231832..767346;
    let mut num_possible = 0;
    for n in range {
        if satisfies_part2(n.to_string()) {
            num_possible += 1;
        }
    }
    num_possible
}

fn satisfies_part1(n: String) -> bool {
    two_adjacent_are_same(&n) && never_decrease(&n)
}

fn satisfies_part2(n: String) -> bool {
    two_adjacent_are_same_strict(&n) && never_decrease(&n)
}

fn two_adjacent_are_same(n: &str) -> bool {
    let skip = n.chars().skip(1);
    n.chars().zip(skip).any(|(a, b)| a == b)
}

fn two_adjacent_are_same_strict(n: &str) -> bool {
    let last_two = n.chars().zip(n.chars().skip(1));
    let next_two = n.chars().skip(2).zip(n.chars().skip(3));
    last_two
        .zip(next_two)
        .any(|((a, b), (c, d))| c == b && c != a && c != d)
        || (n.chars().nth(0) == n.chars().nth(1) && n.chars().nth(1) != n.chars().nth(2))
        || ((n.chars().nth(n.chars().count() - 1) == n.chars().nth(n.chars().count() - 2))
            && (n.chars().nth(n.chars().count() - 2) != n.chars().nth(n.chars().count() - 3)))
}

/// Returns whether each digit is >= the last one.
fn never_decrease(n: &str) -> bool {
    let skip = n.chars().skip(1);
    n.chars().zip(skip).all(|(a, b)| a <= b)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_two_same() {
        assert_eq!(two_adjacent_are_same("122345"), true);
        assert_eq!(two_adjacent_are_same("111123"), true);
        assert_eq!(two_adjacent_are_same("135679"), false);

        assert_eq!(two_adjacent_are_same("111111"), true);
        assert_eq!(two_adjacent_are_same("223450"), true);
        assert_eq!(two_adjacent_are_same("123789"), false);
    }

    #[test]
    fn test_two_same_strict() {
        assert_eq!(two_adjacent_are_same_strict("122345"), true);
        assert_eq!(two_adjacent_are_same_strict("111123"), false);
        assert_eq!(two_adjacent_are_same_strict("135679"), false);

        assert_eq!(two_adjacent_are_same_strict("111111"), false);
        assert_eq!(two_adjacent_are_same_strict("223450"), true);
        assert_eq!(two_adjacent_are_same_strict("123789"), false);
    }

    #[test]
    fn test_never_decrease() {
        assert_eq!(never_decrease("122345"), true);
        assert_eq!(never_decrease("111123"), true);
        assert_eq!(never_decrease("135679"), true);

        assert_eq!(never_decrease("111111"), true);
        assert_eq!(never_decrease("223450"), false);
        assert_eq!(never_decrease("123789"), true);
    }

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 1330);
        assert_eq!(part2(), 876);
    }
}
