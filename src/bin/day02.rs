use advent::input::read_day_csv;
use advent::intcode::Computer;

fn main() {
    println!("part 1: {}", part1());
    println!("part 2: {}", part2());
}

fn part1() -> isize {
    nv(12, 2)
}

fn part2() -> isize {
    let desired = 19690720;
    for noun in 0..99 {
        for verb in 0..99 {
            if nv(noun, verb) == desired {
                return (100 * noun) + verb;
            }
        }
    }
    panic!("none");
}

fn nv(noun: isize, verb: isize) -> isize {
    let mut inp: Vec<isize> = read_day_csv("2");
    inp[1] = noun;
    inp[2] = verb;
    let mut cpu = Computer::new();
    cpu.load_in(inp);
    cpu.run()[0]
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_answers() {
        assert_eq!(part1(), 3765464);
        assert_eq!(part2(), 7610);
    }
}
