let
  moz_overlay = import (builtins.fetchTarball https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz);
  pkgs = import <nixpkgs> { overlays = [ moz_overlay ]; };
in with pkgs;
mkShell {
  name = "adventofcode2019";
  buildInputs = [
    rustChannels.stable.rust
    rls
  ];
  RUST_SRC_PATH = "${rustChannels.stable.rust-src}/lib/rustlib/src/rust/src";
  shellHook = ''
    export PATH="$PWD/.cargo/bin:$PATH"
  '';
}
